#BullseyeTransformationBuild live passwd: live

#download

wget https://codeberg.org/Chebur/TransformationBuild/raw/branch/main/bullseye-transformation.tar.gz

#unzip

tar xvf bullseye-transformation.tar.gz && rm -rf bullseye-transformation.tar.gz 

#install requirements for the build system

sudo apt install -y live-build

#open folder

cd bullseye-transformation

#living system language

lb config --bootappend-live "boot=live components locales=ru_UA.UTF-8"

#sets the base name of the image. Defaults to live-image.

lb config --image-name BullseyeTransformation

#give a command live system Live Installer  Graphical Installer  Установка мышкой   

lb config -d bullseye --debian-installer live --debian-installer-distribution bullseye --archive-areas "main contrib non-free" --debootstrap-options "--variant=minbase"

#give a command live system Live Installer  Graphical Installer  Установка клавиатурой   

lb config -d bullseye --debian-installer live --debian-installer-distribution bullseye --debian-installer-gui false --archive-areas "main contrib non-free" --debootstrap-options "--variant=minbase" 

#give a command live system Live System 

lb config -d bullseye --debian-installer-gui false --archive-areas "main contrib non-free" --debootstrap-options "--variant=minbase" 

#build the image

sudo lb build

Готовый образ 764,0 МиБ

#################################

#BullseyeRefractaInstallerBuild live passwd: live

#download

wget https://codeberg.org/Chebur/TransformationBuild/raw/branch/main/Rbullseye-transformation.tar.gz

#unzip

tar xvf Rbullseye-transformation.tar.gz && rm -rf Rbullseye-transformation.tar.gz 

#install requirements for the build system

sudo apt install -y live-build

#open folder

cd Rbullseye-transformation

#living system language

lb config --bootappend-live "boot=live components locales=ru_UA.UTF-8"

#sets the base name of the image. Defaults to live-image.

lb config --image-name BullseyeRefractaInstaller

#give a command live system Live System 

lb config --config lso:live-images::debian 

#build the image

sudo lb build

Готовый образ 533,0 МиБ

#################################

#BullseyeCalamaresInstaller live passwd: live

#download

wget https://codeberg.org/Chebur/TransformationBuild/raw/branch/main/Cbullseye-transformation.tar.gz

#unzip

tar xvf Cbullseye-transformation.tar.gz && rm -rf Cbullseye-transformation.tar.gz 

#install requirements for the build system

sudo apt install -y live-build

#open folder

cd Cbullseye-transformation

#living system language

lb config --bootappend-live "boot=live components locales=ru_UA.UTF-8"

#sets the base name of the image. Defaults to live-image.

lb config --image-name BullseyeCalamaresInstaller

#give a command live system Live System 

lb config --config lso:live-images::debian 

#build the image

sudo lb build

Готовый образ 642,0 МиБ

#################################

#BookwornTransformationBuild live passwd: live

#download

wget https://codeberg.org/Chebur/TransformationBuild/raw/branch/main/bookworm-transformation.tar.gz

#unzip

tar xvf bookworm-transformation.tar.gz && rm -rf bookworm-transformation.tar.gz

#install requirements for the build system

sudo apt install -y live-build

#open folder

cd bookworm-transformation

#living system language

lb config --bootappend-live "boot=live components locales=ru_UA.UTF-8"

#sets the base name of the image. Defaults to live-image.

lb config --image-name BookwornTransformation

#give a command live system Live Installer  Graphical Installer  Установка мышкой   

lb config -d bookworm --debian-installer live --debian-installer-distribution bookworm --archive-areas "main contrib non-free" --debootstrap-options "--variant=minbase"

#give a command live system Live Installer  Graphical Installer  Установка клавиатурой   

lb config -d bookworm --debian-installer live --debian-installer-distribution bookworm --debian-installer-gui false --archive-areas "main contrib non-free" --debootstrap-options "--variant=minbase" 

#give a command live system Live System 

lb config -d bookworm --debian-installer-gui false --archive-areas "main contrib non-free" --debootstrap-options "--variant=minbase" 

#build the image

sudo lb build

Готовый образ 1015,0 МиБ

#################################

#BookwornCalamaresInstaller live passwd: live

#download

wget https://codeberg.org/Chebur/TransformationBuild/raw/branch/main/Cbookworm-transformation.tar.gz

#unzip

tar xvf Cbookworm-transformation.tar.gz && rm -rf Cbookworm-transformation.tar.gz

#install requirements for the build system

sudo apt install -y live-build

#open folder

cd Cbookworm-transformation

#living system language

lb config --bootappend-live "boot=live components locales=ru_UA.UTF-8"

#sets the base name of the image. Defaults to live-image.

lb config --image-name BookwornCalamaresInstaller

#give a command live system Live System 

lb config --config lso:live-images::debian 

#build the image

sudo lb build

Готовый образ 759,0 МиБ